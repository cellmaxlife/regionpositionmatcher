﻿namespace RegionPositionMatcher
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.regionFile1 = new System.Windows.Forms.TextBox();
            this.regionFile2 = new System.Windows.Forms.TextBox();
            this.btnMatch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.HitIndex1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.XPos1 = new System.Windows.Forms.TextBox();
            this.YPos1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.YPos2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.XPos2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.HitIndex2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.RgnFile1 = new System.Windows.Forms.Button();
            this.RgnFile2 = new System.Windows.Forms.Button();
            this.Distance = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // regionFile1
            // 
            this.regionFile1.Location = new System.Drawing.Point(123, 27);
            this.regionFile1.Name = "regionFile1";
            this.regionFile1.ReadOnly = true;
            this.regionFile1.Size = new System.Drawing.Size(574, 20);
            this.regionFile1.TabIndex = 1;
            // 
            // regionFile2
            // 
            this.regionFile2.Location = new System.Drawing.Point(123, 113);
            this.regionFile2.Name = "regionFile2";
            this.regionFile2.ReadOnly = true;
            this.regionFile2.Size = new System.Drawing.Size(574, 20);
            this.regionFile2.TabIndex = 3;
            // 
            // btnMatch
            // 
            this.btnMatch.Location = new System.Drawing.Point(32, 200);
            this.btnMatch.Name = "btnMatch";
            this.btnMatch.Size = new System.Drawing.Size(75, 23);
            this.btnMatch.TabIndex = 4;
            this.btnMatch.Text = "Match Regions";
            this.btnMatch.UseVisualStyleBackColor = true;
            this.btnMatch.Click += new System.EventHandler(this.btnMatch_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Hit Index";
            // 
            // HitIndex1
            // 
            this.HitIndex1.Location = new System.Drawing.Point(96, 69);
            this.HitIndex1.Name = "HitIndex1";
            this.HitIndex1.Size = new System.Drawing.Size(51, 20);
            this.HitIndex1.TabIndex = 6;
            this.HitIndex1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HitIndex1_KeyUp);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(175, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "X";
            // 
            // XPos1
            // 
            this.XPos1.Location = new System.Drawing.Point(195, 69);
            this.XPos1.Name = "XPos1";
            this.XPos1.ReadOnly = true;
            this.XPos1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.XPos1.Size = new System.Drawing.Size(69, 20);
            this.XPos1.TabIndex = 8;
            // 
            // YPos1
            // 
            this.YPos1.Location = new System.Drawing.Point(311, 69);
            this.YPos1.Name = "YPos1";
            this.YPos1.ReadOnly = true;
            this.YPos1.Size = new System.Drawing.Size(69, 20);
            this.YPos1.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(291, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Y";
            // 
            // YPos2
            // 
            this.YPos2.Location = new System.Drawing.Point(314, 152);
            this.YPos2.Name = "YPos2";
            this.YPos2.ReadOnly = true;
            this.YPos2.Size = new System.Drawing.Size(69, 20);
            this.YPos2.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(294, 155);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Y";
            // 
            // XPos2
            // 
            this.XPos2.Location = new System.Drawing.Point(198, 152);
            this.XPos2.Name = "XPos2";
            this.XPos2.ReadOnly = true;
            this.XPos2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.XPos2.Size = new System.Drawing.Size(69, 20);
            this.XPos2.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(178, 155);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "X";
            // 
            // HitIndex2
            // 
            this.HitIndex2.Location = new System.Drawing.Point(99, 152);
            this.HitIndex2.Name = "HitIndex2";
            this.HitIndex2.ReadOnly = true;
            this.HitIndex2.Size = new System.Drawing.Size(51, 20);
            this.HitIndex2.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(35, 156);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Hit Index";
            // 
            // RgnFile1
            // 
            this.RgnFile1.Location = new System.Drawing.Point(32, 27);
            this.RgnFile1.Name = "RgnFile1";
            this.RgnFile1.Size = new System.Drawing.Size(75, 23);
            this.RgnFile1.TabIndex = 17;
            this.RgnFile1.Text = "Rgn File1";
            this.RgnFile1.UseVisualStyleBackColor = true;
            this.RgnFile1.Click += new System.EventHandler(this.RgnFile1_Click);
            // 
            // RgnFile2
            // 
            this.RgnFile2.Location = new System.Drawing.Point(32, 111);
            this.RgnFile2.Name = "RgnFile2";
            this.RgnFile2.Size = new System.Drawing.Size(75, 23);
            this.RgnFile2.TabIndex = 18;
            this.RgnFile2.Text = "Rgn File2";
            this.RgnFile2.UseVisualStyleBackColor = true;
            this.RgnFile2.Click += new System.EventHandler(this.RgnFile2_Click);
            // 
            // Distance
            // 
            this.Distance.Location = new System.Drawing.Point(465, 152);
            this.Distance.Name = "Distance";
            this.Distance.ReadOnly = true;
            this.Distance.Size = new System.Drawing.Size(69, 20);
            this.Distance.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(411, 155);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Distance";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 235);
            this.Controls.Add(this.Distance);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RgnFile2);
            this.Controls.Add(this.RgnFile1);
            this.Controls.Add(this.YPos2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.XPos2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.HitIndex2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.YPos1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.XPos1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.HitIndex1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnMatch);
            this.Controls.Add(this.regionFile2);
            this.Controls.Add(this.regionFile1);
            this.Name = "Form1";
            this.Text = "Region Position Matcher (V1)";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox regionFile1;
        private System.Windows.Forms.TextBox regionFile2;
        private System.Windows.Forms.Button btnMatch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox HitIndex1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox XPos1;
        private System.Windows.Forms.TextBox YPos1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox YPos2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox XPos2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox HitIndex2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button RgnFile1;
        private System.Windows.Forms.Button RgnFile2;
        private System.Windows.Forms.TextBox Distance;
        private System.Windows.Forms.Label label1;
    }
}

