﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegionPositionMatcher
{
    public partial class Form1 : Form
    {
        private const int MIN_RADIUS = 50;
        private string defaultRegionFolder = @"C:\";
        private List<RegionData> regions1 = new List<RegionData>();
        private List<RegionData> regions2 = new List<RegionData>();
        private int hitIndex = 0;

        public Form1()
        {
            InitializeComponent();
            regionFile1.Text = "";
            regionFile2.Text = "";
            HitIndex1.Text = "";
            XPos1.Text = "";
            YPos1.Text = "";
            HitIndex2.Text = "";
            XPos2.Text = "";
            YPos2.Text = "";
        }

        private void btnMatch_Click(object sender, EventArgs e)
        {
            HitIndex2.Text = "";
            XPos2.Text = "";
            YPos2.Text = "";
            Distance.Text = "";
            if ((regions1.Count > 0) && (regions2.Count > 0))
            {
                if ((hitIndex > 0) && (hitIndex <= regions1.Count))
                {    
                    int matchIndex = -1;
                    int x1 = Convert.ToInt32(XPos1.Text);
                    int y1 = Convert.ToInt32(YPos1.Text);
                    int minDist = MIN_RADIUS;
                    for (int i=0; i<regions2.Count; i++)
                    {
                        int dx = x1 - regions2[i].m_x0;
                        int dy = y1 - regions2[i].m_y0;
                        int dist = (int)Math.Sqrt((double)(dx * dx + dy * dy));
                        if (dist < minDist)
                        {
                            matchIndex = i;
                            minDist = dist;
                        }
                    }
                
                    if (matchIndex > -1)
                    {
                        HitIndex2.Text = (matchIndex + 1).ToString();
                        XPos2.Text = regions2[matchIndex].m_x0.ToString();
                        YPos2.Text = regions2[matchIndex].m_y0.ToString();
                        Distance.Text = minDist.ToString();
                    }
                    else
                    {
                        string message = "Failed to find HitIndex1 Match in Rgn File2";
                        MessageBox.Show(message);
                    }
                }
                else
                {
                    string message = "Please set HitIndex1 to a number greater than 0 and less or equal to " + regions1.Count.ToString();
                    MessageBox.Show(message);
                }
            }
            else
            {
                string message = "Not both region lists have non-zero count, (list1.count=" + regions1.Count + ", list2.count=" + regions2.Count + ")";
                MessageBox.Show(message);
            }
        } 

        private void LoadRegionDataFromFile(string filename, List<RegionData> regionList)
        {
            const string TAG1 = "0 1, 1 ";
            const string TAG2 = ", 2 ";
            const string TAG3 = ", ";

            using (System.IO.StreamReader file = new System.IO.StreamReader(filename))
            {
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    int index = line.IndexOf(TAG1);
                    if (index > -1)
                    {
                        line = line.Substring(TAG1.Length);
                        index = line.IndexOf(TAG2);
                        if (index > -1)
                        {
                            uint color = Convert.ToUInt32(line.Substring(0, index));
                            line = line.Substring(index + TAG2.Length);
                            index = line.IndexOf(' ');
                            if (index > -1)
                            {
                                int x0 = Convert.ToInt32(line.Substring(0, index));
                                line = line.Substring(index + 1);
                                index = line.IndexOf(TAG3);
                                if (index > -1)
                                {
                                    int y0 = Convert.ToInt32(line.Substring(0, index));
                                    RegionData data = new RegionData();
                                    data.m_x0 = x0;
                                    data.m_y0 = y0;
                                    data.m_color = color;
                                    regionList.Add(data);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void RgnFile1_Click(object sender, EventArgs e)
        {
            regionFile1.Text = "";
            XPos1.Text = "";
            YPos1.Text = "";
            XPos2.Text = "";
            YPos2.Text = "";
            regions1.Clear();
            OpenFileDialog openRegionFile = new OpenFileDialog();
            openRegionFile.Filter = "Region File1 (*.rgn) | *.rgn";
            openRegionFile.InitialDirectory = defaultRegionFolder;
            if (openRegionFile.ShowDialog() == DialogResult.OK)
            {
                regionFile1.Text = openRegionFile.SafeFileName;
                LoadRegionDataFromFile(openRegionFile.FileName, regions1);
                string Pathname = openRegionFile.FileName;
                int index = Pathname.LastIndexOf('\\');
                Pathname = Pathname.Substring(0, index);
                defaultRegionFolder = Pathname;
            }
        }

        private void RgnFile2_Click(object sender, EventArgs e)
        {
            regionFile2.Text = "";
            XPos2.Text = "";
            YPos2.Text = "";
            regions2.Clear();
            OpenFileDialog openRegionFile = new OpenFileDialog();
            openRegionFile.Filter = "Region File1 (*.rgn) | *.rgn";
            openRegionFile.InitialDirectory = defaultRegionFolder;
            if (openRegionFile.ShowDialog() == DialogResult.OK)
            {
                regionFile2.Text = openRegionFile.SafeFileName;
                LoadRegionDataFromFile(openRegionFile.FileName, regions2);
                string Pathname = openRegionFile.FileName;
                int index = Pathname.LastIndexOf('\\');
                Pathname = Pathname.Substring(0, index);
                defaultRegionFolder = Pathname;
            }
        }

        private void HitIndex1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                hitIndex = Convert.ToInt32(HitIndex1.Text);
                if ((hitIndex > 0) && (hitIndex <= regions1.Count))
                {
                    XPos1.Text = regions1[hitIndex - 1].m_x0.ToString();
                    YPos1.Text = regions1[hitIndex - 1].m_y0.ToString();
                    HitIndex2.Text = "";
                    XPos2.Text = "";
                    YPos2.Text = "";
                    Distance.Text = "";
                }
                else
                {
                    XPos1.Text = "";
                    YPos1.Text = "";
                }
            }
        }
    }
}
